#pragma once
#include "Pose.h"

class RobotInterface
{
private:
	Pose* position;
	int state;
public:
	RobotInterface();
	~RobotInterface();
	void turnLeft();
	void turnRight();
	void forward(float speed);
	void print();
	void backward(float speed);
	Pose getPose();
	void setPose(Pose);
	void stopTurn();
	void stopMove();
	void updateSensor();

};