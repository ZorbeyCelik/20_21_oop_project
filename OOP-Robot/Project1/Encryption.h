#pragma once
#include <iostream>
using namespace std;
class Encryption
{
public:
	
	Encryption();
	~Encryption();
	int encrypt(int);
	int decrypt(int);
};