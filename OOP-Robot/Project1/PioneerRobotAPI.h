#pragma once
#include "Aria/Aria.h"
#include <iostream>
class PioneerRobotAPI {
public:
	enum DIRECTION {
		left = -1,
		forward = 0,
		right = 1
	};

	PioneerRobotAPI();
	~PioneerRobotAPI();

	
	bool connect();
	
	bool disconnect();
	
	void moveRobot(float speed);
	
	void stopRobot();
	
	void turnRobot(DIRECTION dir);
	
	void getSonarRange(float ranges[]) const;
	
	float getX() const;
	
	float getY() const;
	
	float getTh() const;
	
	void setPose(float x, float y, float th);
	
	void getLaserRange(float ranges[]) const;
	
	virtual void updateRobot() {}
	
	void setRobot(PioneerRobotAPI *robotapi);
};