#pragma once
struct Node
{
	Node* next;
	Pose pose;
};
class Pose		
{
private:
	float x, y, th;
public:
	Pose();	
	~Pose();
	float getX();
	void setX(float);
	float getY();
	void setY(float);
	void setTh(float);
	float getTh();
	bool operator==(Pose& const other);
	Pose operator+(Pose& const other);
	Pose operator-(Pose& const other);
	Pose& operator+=(Pose& const other);
	Pose& operator-=(Pose& const other);
	bool operator<(Pose& const other);
	void getPose(float& _x, float& _y, float& _th);
	void setPose(float _x, float _y, float _th);
	float findDistanceTo(Pose pos); 
	float findAngleTo(Pose pos); 

};