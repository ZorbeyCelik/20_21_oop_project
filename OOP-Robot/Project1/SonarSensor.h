#include "PioneerRobotAPI.h"
#include "RangeSensor.h"
#pragma once

class SonarSensor:public RangeSensor									
public:
	SonarSensor();
	~SonarSensor();
	
	float getRange(int index);
	
	float getMax(int& index);
	
	float getMin(int& index);
	
	void updateSensor(float[]);
	
	float getAngle(int);
	float getClosestRange(float startAngle, float endAngle, float& angle);
};