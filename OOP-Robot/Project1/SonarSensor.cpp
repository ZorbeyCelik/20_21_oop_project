#include "SonarSensor.h"
#include "PioneerRobotAPI.h"


float SonarSensor::getRange(int i)																												// H�seyin Ko�er 152120181103
{
	return ranges[i];
}

float SonarSensor::getMax(int& a)
{
	int max=ranges[0];
	int index = 0;
	for (int i = 0; i < 16; i++) {
		if (ranges[i] > max) {
			max = ranges[i];
			index = i;
		}
	}
	a = index;
	return max;
}

float SonarSensor::getMin(int& a)
{
	int min = ranges[0];
	int index = 0;
	for (int i = 0; i < 16; i++) {
		if (ranges[i] < min) {
			min = ranges[i];
			index = i;
		}
	}
	a = index;
	return min;
}

void SonarSensor::updateSensor(float xranges[])
{
	for (int i = 0; i < 16; i++) {
		ranges[i] = xranges[i];
	}
}

float SonarSensor::operator[](int index)
{
	return index;
}

float SonarSensor::getAngle(int a)
{
	int angle;
	if (a < 8) {
		angle = 10 + 20 * a;
	}
	else {
		angle = -10 - 20 * a;
	}
	
	
	return angle;
}
