#pragma once
#include <iostream>
#include <string>
#include <fstream>
using namespace std;
class Record
{
private:
	string fileName;
	fstream file;
public:
	Record();
	~Record();
	bool openFile();
	bool closeFile();
	void setFileName(string name);
	string readLine();
	bool writeLine(string str);
	Record& operator<<();
	Record& operator>>();
	
	


};