#pragma once
#include "PioneerRobotAPI.h"
class RangeSensor
{
private:
	float ranges[1000];
	PioneerRobotAPI* robotAPI;
public:
	RangeSensor();
	~RangeSensor();
	float getRange(int index);
	float getMax(int& index);
	float getMin(int& index);
	void updateSensor(float ranges[]);
	float operator[](int i);
	float getAngle(int index);
	float getClosestRange(float startAngle, float endAngle, float& angle);

};