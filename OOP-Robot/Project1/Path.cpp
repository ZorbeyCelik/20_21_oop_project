#include "Path.h"
Path::Path()																																	//ibrahim yavuz 152120181028
{
	number = 0;
}

Path::~Path()
{
}
void Path::addPos(Pose pose) {
	Node* new_node = new Node();
	new_node->pose = pose;
	new_node->next = NULL;
	if (head == NULL) {
		head = tail = new_node;
	}
	else {
		tail->next = new_node;
		tail = new_node;
	}
	number++;
}
void Path::print() {
	int i = 1;
	Node* tmp = head;
	while (tmp) {
		std::cout << i << ". x:" << tmp->pose.getX() << " y:" << tmp->pose.getY() << " angle:" << tmp->pose.getTh() << std::endl;
		i++;
		tmp = tmp->next;
	}
}
Pose Path::operator[](int i) {
	return getPos(i);
}
Pose Path::getPos(int index) {
	int i = 1;
	Node* tmp = head;
	while (tmp) {
		if (i == index) {
			return tmp->pose;
		}
		i++;
		tmp = tmp->next;
	}
}
bool Path::removePos(int index) {
	int i = 1;
	Node* q = NULL;
	Node* p = head;
	while (p != NULL && i != index) {
		q = p;
		p = p->next;
		i++;
	}
	if (p == NULL) {
		return false;
	}
	if (q == NULL) {
		head = p->next;
	}
	else {
		q->next = p->next;
	}
	delete p;
}
bool Path::insertPos(int index, Pose pose) {
	Node* new_node = new Node();
	int i = 1;
	new_node->pose = pose;
	Node* p = head;
	Node* tmp;
	while (p != NULL && i != index) {
		p = p->next;
		i++;
	}
	if (p == NULL) {
		return false;
	}
	new_node->next = p->next;
	p->next = new_node;
}
void Path::operator>>(Pose oth) {
	addPos(oth);
}
void Path::operator<<(Pose oth) {
	int i = 1;
	Node* tmp = head;
	while (tmp) {
		std::cout << i << ". x:" << tmp->pose.getX() << " y:" << tmp->pose.getY() << " angle:" << tmp->pose.getTh() << std::endl;
		i++;
		tmp = tmp->next;
	}
}
