#include "Record.h"

Record::Record()
{
}

Record::~Record()
{
}

bool Record::openFile()
{
    fstream file("input.txt", ios::in);
    fstream file("output.txt", ios::out);
    if (file.is_open())
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool Record::closeFile()
{
    file.close();
    if (!file.is_open())
    {
        return true;
    }
    else
    {
        return false;
    }
}

void Record::setFileName(string name)
{
    fileName = name;
}

string Record::getFileName()
{
    return fileName;
}

string Record::readLine()
{
    char words[200];
    while (!file.eof())
    {
        file.getline(words, 200);
    }
    return words;
}

bool Record::writeLine(string str)
{
    if (file.is_open())
    {
        file << str;
        return true;
    }
    else
    {
        cout << "Files is not open!";
        return false;
    }
}
