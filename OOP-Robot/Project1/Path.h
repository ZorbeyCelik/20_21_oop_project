#pragma once
#include <iostream>																																	
#include "Pose.h"
class Path
{
private:
	Node* tail;
	Node* head;
	int number;
public:
	Path();
	~Path();
	void addPos(Pose pose);
	void print();
	Pose operator[](int i);
	Pose getPos(int index);
	bool removePos(int index);
	bool insertPos(int index, Pose pose);
	void operator<<(Pose oth);
	void operator>>(Pose oth);
};