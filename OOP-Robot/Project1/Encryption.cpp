#include "Encryption.h"
//ZORBEY �EL�K (8.01.2021)

Encryption::Encryption()	///First digits values are here
{
	for (int i = 0; i < 4; i++)
	{
		digits[i] = 0;
	}
}

Encryption::~Encryption()
{
	
}

void Encryption::encrypt(int digit) ///0 ile ba�layan say�larda hata veriliyor
{
	int* ptr = digits;
	if(digit > 999 && digit <= 9999)
	{
		for (int i = 3; i >=0; i--) {
			ptr[i] = digit % 10;
			digit = digit / 10;
		}
		for (int i = 0; i < 4; i++)  ///First work Encryption(value+7)
		{
			ptr[i] = (ptr[i] + 7) % 10;
		}
		int a = ptr[0];           ///Change 1. and 3. digits with each other
		ptr[0] = ptr[2];
		ptr[2] = a;

		int b = ptr[1];		     ///Change 2. and 4. digits with each other
		ptr[1] = ptr[3];
		ptr[3] = b;

		cout << "After Encryption Digits are:";
		for (int x = 0; x < 4; x++)  ///Print digits value
		{
			cout << ptr[x];
		}

		cout << endl;
	}
	else
	{
		cout << "Error.Wrong values.";
	}
}

void Encryption::decrypt(int digit)	///Solution Encryption
{
	int* ptr = digits;
	if (digit > 999 && digit <= 9999)
	{
		for (int i = 3; i >= 0; i--) {
			ptr[i] = digit % 10;
			digit = digit / 10;
		}
		int a = digits[2];		///Change 1. and 3. digits with each other
		digits[2] = digits[0];
		digits[0] = a;

		int b = digits[3];		///Change 2. and 4. digits with each other
		digits[3] = digits[1];
		digits[1] = b;

		for (int i = 0; i < 4; i++)
		{
			if (digits[i] == 7 || digits[i] == 8 || digits[i] == 9)
			{
				digits[i] -= 7;
			}
			else
			{
				digits[i] += 3;
			}
		}
		cout << "After Reverse Encryption digits are:";
		for (int x = 0; x < 4; x++)
		{
			cout << digits[x];
		}
	}
	else
	{
		cout << "Error.Wrong values.";
	}
	cout << endl;
}