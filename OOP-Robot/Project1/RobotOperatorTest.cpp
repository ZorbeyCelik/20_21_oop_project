#include <iostream>
#include "RobotOperator.h"

using namespace std;

///ZORBEY �EL�K (9.01.2021)

int main() {
	RobotOperator test1 ;
	test1.encryptCode(1234);
	test1.encryptCode(2222);

	test1.decryptCode(1000);
	test1.decryptCode(9999);

	test1.checkAccessCode(2222);
	test1.print();
	return 0;
}