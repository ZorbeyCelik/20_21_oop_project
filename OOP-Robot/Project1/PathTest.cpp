#include"Pose.h"
#include"Path.h"
#include<iostream>																																	//ibrahim yavuz 152120181028
using namespace std;
int main() {
	Pose pose1,pose2,pose3,pose4,pose5;
	Path p1;
	pose1.setX(19);
	pose1.setY(28);
	pose1.setTh(45);
	p1.addPos(pose1);
	pose2.setX(21);
	pose2.setY(26);
	pose2.setTh(30);
	p1.addPos(pose2);
	pose3.setX(17);
	pose3.setY(81);
	pose3.setTh(90);
	p1.addPos(pose3);
	pose4.setX(112);
	pose4.setY(145);
	pose4.setTh(163);
	p1.addPos(pose4);
	p1.print();
	cout << endl;
	cout << "x of p[1]:" << p1[1].getX() <<endl<< "y of p[2]:" << p1[2].getY() << endl;
	cout << "x of 2. index:" << p1.getPos(2).getX() <<endl<< "y of 3.index:" << p1.getPos(3).getY() << endl;
	p1.removePos(2);
	cout << "\n2.index was removed!!!!!\n";
	p1.print();
	cout << endl;
	p1.insertPos(1, pose2);
	cout << "2.index was retaken!!!!!\n";
	p1.print();
	cout << endl;
	pose5.setX(221);
	pose5.setY(280);
	pose5.setTh(356);
	p1.operator>>(pose5);
	p1.operator<<(pose5);
	return 0;
}