#pragma once
#include<iostream>
#include "Pose.h"
#include"PioneerRobotAPI.h"

class RobotControl
{
public:
	RobotControl();
	~RobotControl();
	void turnLeft();
	void turnRight();
	void forward(float speed);
	void print();
	void backward(float speed);
	Pose getPose();
	void setPose(Pose);
	void stopTurn();
	void stopMove();
	bool addToPath();
	bool clearPath();
	bool recordPathToFile();
	bool openAccess(int);
	bool closeAccess(int);

};