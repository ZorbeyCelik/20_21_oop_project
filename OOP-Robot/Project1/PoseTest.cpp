#pragma once
#include"Pose.h"
#include<iostream>																												//ibrahim yavuz 152120181028
using namespace std;
int main() {
	Pose p1, p2;
	p1.setX(19);
	p1.setY(28);
	p1.setTh(45);
	p2.setX(21);
	p2.setY(26);
	p2.setTh(30);
	cout << "p1::: x:" << p1.getX() << " y:" << p1.getY() << " angle:" << p1.getTh() << endl;
	cout << "p2::: x:" << p2.getX() << " y:" << p2.getY() << " angle:" << p2.getTh() << endl;
	float a, b, c;
	p1.getPose(a, b, c);
	cout << "p1::: x:" << a << " y:" << b << " angle:" <<c << endl;
	p1.setPose(37, 16, 65);
	cout << "::::::::p1.setPose(37,16, 65):::::::::\n";
	cout << "p1::: x:" << p1.getX() << " y:" << p1.getY() << " angle:" << p1.getTh() << endl;
	cout << "p2::: x:" << p2.getX() << " y:" << p2.getY() << " angle:" << p2.getTh() << endl;
	cout << "Distance:" << p1.findDistanceTo(p2)<<endl;
	cout << "Angle:" << p1.findAngleTo(p2)<<endl;
	return 0;
}