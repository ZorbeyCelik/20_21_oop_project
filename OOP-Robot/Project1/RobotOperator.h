#pragma once	
#include "Encryption.h"
#include <iostream>
#include <string>
using namespace std;
class RobotOperator:private Encryption
{
private:
	string name;
	string surname;
	bool accessState;
	unsigned int accessCode;
	int encryptCode(int);
	int decryptCode(int);
public:
	RobotOperator();
	RobotOperator(int);
	~RobotOperator();
	bool checkAccessCode(int);
	void print();
	
	

};