#include "RobotOperator.h"

RobotOperator::RobotOperator()
{
	name = "";
	surname = "";
	accessCode = 9999;
}
RobotOperator::RobotOperator(int access)
{
	accessCode = encryptCode(access);
}

RobotOperator::~RobotOperator()
{
}

bool RobotOperator::checkAccessCode(int access)
{
	if (decryptCode(accessCode) == access)
	{
		accessState = true;
	}
	else
	{
		return accessState = false;
	}
}

void RobotOperator::print()
{
	cout << "Name:" << name << endl;
	cout << "Surname:" << surname << endl;
	cout << "Acces State:" << accessState << endl;
}

int RobotOperator::encryptCode(int code)
{	
	encrypt(code);
	return 0;
}

int RobotOperator::decryptCode(int code)
{
	decrypt(code);
	return 0;
}
