#include "PioneerRobotAPI.h"
#include "RangeSensor.h"
#pragma once
class LaserSensor :public RangeSensor																																		
{
public:
	LaserSensor();
	~LaserSensor();
	float getRange(int index);
	void updateSensor(float ranges[]);
	float getMax(int& index);
	float getMin(int& index);
	float getAngle(int index);
	float getClosestRange(float startAngle, float endAngle, float& angle);
};