#include "LaserSensor.h"																						// H�seyin Ko�er 152120181103
#include "PioneerRobotAPI.h"

float LaserSensor::getRange(int index)
{
	return ranges[index];
}

void LaserSensor::updateSensor(float xranges[])
{
	for (int i = 0; i < 181; i++) {
		ranges[i] = xranges[i];
	}
}

float LaserSensor::getMax(int& index)
{
	int max = ranges[0];
	int a = 0;
	for (int i = 0; i < 181; i++) {
		if (ranges[i] > max) {
			max = ranges[i];
			index = i;
		}
	}
	index = a;
	return max;
}

float LaserSensor::getMin(int& index)
{
	int min = ranges[0];
	int a = 0;
	for (int i = 0; i < 181; i++) {
		if (ranges[i] < min) {
			min = ranges[i];
			index = i;
		}
	}
	index = a;
	return min;
}

float LaserSensor::operator[](int index)
{
	return index;
}

float LaserSensor::getAngle(int index)
{
	return index;
}

float LaserSensor::getClosestRange(float startAngle, float endAngle, float& angle)
{
	int min=startAngle;
	for (int i = startAngle; i < endAngle; i++) {
		if (ranges[i] < ranges[min]) {
			min = i;
		}
	}
	angle = min;
	return ranges[min];
}
