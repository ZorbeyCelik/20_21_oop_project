#pragma once
#include"PioneerRobotAPI.h"
#include"RobotInterface.h"

class PioneerRobotInterface:public RobotInterface
{
private:
	PioneerRobotAPI* robotAPI;
public:
	PioneerRobotInterface();
	~PioneerRobotInterface();
	void turnLeft();
	void turnRight();
	void forward(float speed);
	void print();
	void backward(float speed);
	Pose getPose();
	void setPose(Pose);
	void stopTurn();
	void stopMove();
	void updateSensor();

};