#include "Pose.h"
#include<math.h>																																					//ibrahim yavuz 152120181028
Pose::Pose()
{
}

Pose::~Pose()
{
}

float Pose::getX() {
	return x;
}
void Pose::setX(float x) {
	this->x = x;
}
float Pose::getY() {
	return y;
}
void Pose::setY(float y) {
	this->y = y;
}
float Pose::getTh() {
	return th;
}
void Pose::setTh(float th) {
	this->th = th;
}
bool Pose::operator==(Pose& const other) {
	/// Empty
	return 0;
}
Pose Pose::operator+(Pose& const other) {
	///Empty
	return other;
}
Pose Pose::operator-(Pose& const other) {
	///Empty
	return other;
}
Pose& Pose::operator+=(Pose& const other) {
	///Empty
	return other;
}
Pose& Pose::operator-=(Pose& const other) {
	///Empty
	return other;
}
bool Pose::operator<(Pose& const other) {
	///Empty
	return 0;
}
void Pose::getPose(float& _x, float& _y, float& _th) {
	_x = x;
	_y = y;
	_th = th;
}
void Pose::setPose(float _x, float _y, float _th) {
	x = _x;
	y = _y;
	th = _th;
}
float Pose::findDistanceTo(Pose pos) {
	return (float)sqrt(pow((this->x - pos.x), 2) + pow((this->y - pos.y), 2));
}
float Pose::findAngleTo(Pose pos) {
	return this->th + pos.th;
}
